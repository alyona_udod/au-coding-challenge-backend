const express = require('express');
const router = express.Router();
const ctrlRepos = require('../controllers/controllerRepos');

// 3 endpoints with callbacks. Only routs.

router.get('/', ctrlRepos.getAllRepos);

router.get('/page=:id', ctrlRepos.getReposByPage);

router.post('/', ctrlRepos.addRepo);

module.exports = router;