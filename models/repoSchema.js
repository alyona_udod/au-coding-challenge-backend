const mongoose = require('mongoose');

const Schema = mongoose.Schema;

// Schema for saving repo object to DB. 
const repoSchema = new Schema({
    full_name: { type: String, require: true },
    html_url: { type: String, require: true },
    description:{ type: String, require: true },
    stargazers_count: { type: Number, require: true },
    langueage: { type: String, require: true },
});

const Repo = mongoose.model('ReposCollection', repoSchema);
module.exports = Repo;