const Repo = require('./repoSchema');

// Functions for working with DB.
module.exports.getAllReposFromDB = function(){
    let mySort = { stargazers_count: -1 }; // sorting by starts. from max till min
    return Repo.find().sort(mySort).exec();
};

module.exports.getReposByPageFromDB = function(id) {
    let mySort = { stargazers_count: -1 };
    let perPage = 10
    let page = id || 1
    return Repo.find()
            .sort(mySort)
            .skip((perPage * page) - perPage)
            .limit(perPage)
            .exec();
};

module.exports.addRepoToDB = function(req){
    const { full_name, html_url, description, stargazers_count, language } = req.body;
    const newRepo = new Repo({ // use Repo Schema for saving repo obj to DB
        full_name,
        html_url,
        description,
        stargazers_count,
        language,
    });
    return newRepo.save();
};