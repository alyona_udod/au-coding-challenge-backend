Simple RESTful API that imports data from GitHub Search API https://developer.github.com/v3/search/

App imports repositories that have been written in Python and have more than 500 stars and saves to MongoDb Collection object with a few fields: 
 - full_name, 
 - html_url, 
 - description, 
 - stargazers_count, 
 - language.

App have 3 endpoints:

GET : http://localhost:8000/api/repo - Server response with such info: total count of repos, total count of pages and array with repos sorted by 'stargazers_count' field (from max till min).

GET: http://localhost:8000/api/repo/page=id - Where id - page number from requist. Server return response with page number, number repos per_page and page's repos (sorted by 'stargazers_count' field).

POST: http://localhost:8000/api/repo - This endpoint using in the begging, when server is already runnign and sending request to GitHub Search Api for creating repo collection on MongoDB.