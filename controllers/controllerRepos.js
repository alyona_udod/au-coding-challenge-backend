const DB = require('../models/reposDB');

// 3 callback functions for 3 endpoints. This functuins take requests, send it to DB and get response from DB. It's like a pipe for info with assigning a status. From view to model, from model to view.

module.exports.getAllRepos = (req, res) => {
    DB
    .getAllReposFromDB()
    .then(data => {
        res
        .status(200)
        .json({total_repos: data.length, total_pages: Math.ceil(data.length/10), repos: data})
    })
    .catch(err =>{
        res
        .status(400)
        .json({err: err.message});
    });
};

module.exports.getReposByPage = (req, res) => {
    DB
    .getReposByPageFromDB(req.params.id)
    .then(data => {
        res
        .status(200)
        .json({page: +req.params.id, per_page: data.length, repos: data})
    })
    .catch(err =>{
        res
        .status(400)
        .json({err: err.message});
    });
};

module.exports.addRepo = (req, res) => {
    DB
    .addRepoToDB(req)
    .then((data) => {
      res
        .status(201)
        .json(data);
    })
    .catch((err) => {
      res
        .status(400)
        .json({err: err.message});
    });
};