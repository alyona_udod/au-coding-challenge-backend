const express = require('express'); 
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const cors = require('cors');
const axios = require('axios');
const reposRoutes = require('./routes/reposRoutes');
require('dotenv').config();
const PORT = process.env.PORT || 8000;

const app = express();
app.use(bodyParser.json()); 
app.use(bodyParser.urlencoded({extended: true}));

app.use(cors());
app.options('*', cors());

app.use('/api/repo', reposRoutes); // in this file only endpoint routs.

mongoose.Promise = global.Promise;
mongoose.set('useNewUrlParser', true);
mongoose.set('useFindAndModify', false);
mongoose.set('useCreateIndex', true);
mongoose.connect(process.env.MONGODB_URL);

// First request to server for knowing how many repos with language:python and start more then 500 GitHub has.
axios.get('https://api.github.com/search/repositories?q=stars:>=500+language:python&per_page=1')
.then(res => getAllRepos(res.data)) // when we have total number of repos we're starting requests chain 
.catch(err => console.log(err));

function getAllRepos(data){
    let totalNumber = Math.ceil(data.total_count/100); // get total pages that we want to get.
    for (let i = 1; i <= totalNumber; i++){
            axios.get(`https://api.github.com/search/repositories?q=stars:>=500+language:python&page=${i}&per_page=100`)
            .then(res => getInfo(res.data, i))
            .catch(err => console.log(err)); // when we get response data sends to func where writes to DB
            // I have problem with req - 'API rate limit exceeded for ***.***.**.***. (But here\'s the good news: Authenticated requests get a higher rate limit. Check out the documentation for more details.)'. I had tried to fix it with setTimeout, but didn't succsed, because code looks like loooks.
    };
};

function getInfo(data, a){
    data.items.map((el,ind,arr) => { // iterating array from req for sending repo info to DB
        let obj = {
            full_name: el.full_name,
            html_url: el.html_url,
            description: el.description,
            stargazers_count: el.stargazers_count,
            langueage: el.language
        };
        axios.post('http://localhost:8000/api/repo', obj, {
            headers: {
                'Content-Type': 'application/json',
            }
          })
        .then(res => console.log(`${a*(arr.indexOf(el)+1)} repo was added.`))
        .catch(err => console.log(err));
    });
};

app.get('/', (req,res) => res.send('hello! this is a server page ^_^')); // only for look on http://localhost:8000 and see something.

app.use((req, res, next) => {
  res
    .status(404)
    .json({err: '404'});
}); // for errors catching from wrong req
 
app.use((err, req, res, next) => {
  res
    .status(500)
    .json({err: '500'});
}); // for errors from server, if something bad with server.

app.listen(PORT, () => console.log(`Server is running on port ${PORT}`)); 